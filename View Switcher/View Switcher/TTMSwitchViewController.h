//
//  TTMSwitchViewController.h
//  View Switcher
//
//  Created by admin on 9/11/14.
//  Copyright (c) 2014 TriThucMoi. All rights reserved.
//

#import <UIKit/UIKit.h>

@class TTMBlueViewController;
@class TTMYellowViewController;

@interface TTMSwitchViewController : UIViewController

@property (strong, nonatomic) TTMBlueViewController *blueViewController;
@property (strong, nonatomic) TTMYellowViewController *yellowViewController;

- (IBAction)switchViews:(id)sender;

@end
