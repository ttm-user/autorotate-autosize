//
//  TTMAppDelegate.h
//  View Switcher
//
//  Created by admin on 9/11/14.
//  Copyright (c) 2014 TriThucMoi. All rights reserved.
//

#import <UIKit/UIKit.h>
@class TTMSwitchViewController;
@interface TTMAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;
@property (strong, nonatomic) TTMSwitchViewController *switchViewController;

@end
