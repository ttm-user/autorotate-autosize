//
//  main.m
//  View Switcher
//
//  Created by admin on 9/11/14.
//  Copyright (c) 2014 TriThucMoi. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "TTMAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([TTMAppDelegate class]));
    }
}
